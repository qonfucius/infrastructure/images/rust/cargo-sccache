ARG RUST_VERSION=rust:alpine
FROM $RUST_VERSION

RUN cargo install --git https://github.com/paritytech/cachepot